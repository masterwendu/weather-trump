require('dotenv').config()
const yaml = require('js-yaml');
const fs = require('fs');
const { create } = require('apisauce')
const randomNumber = require('./helpers/randomNumber');
const fsAsync = fs.promises

const fastify = require('fastify')({ logger: true })

const weatherApi = create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
  headers: { Accept: 'application/json' },
})

const regions = {
  Africa: ['Northern Africa', 'Sub-Saharan Africa'],
  Americas: ['Latin America and the Caribbean', 'Northern America'],
  Asia: ['Central Asia', 'Eastern Asia', 'South-eastern Asia', 'Southern Asia', 'Western Asia'],
  Europe: ['Eastern Europe', 'Northern Europe', 'Southern Europe', 'Western Europe'],
  Oceania: ['Australia and New Zealand', 'Melanesia', 'Micronesia', 'Polynesia'],
}

// Declare a route
fastify.get('/regions', async (request, reply) => {
  return regions
})

fastify.get('/countries/:region/:subRegion', async (request, reply) => {
  const region = request.params.region
  const subRegion = request.params.subRegion
  if (Object.keys(regions).includes(region) && regions[region].includes(subRegion)) {
    return yaml.load(await fsAsync.readFile(`./data/${region}/${subRegion}.yaml`, 'utf8'))
  }
  reply
    .code(404)
    .send({ error: {
      message: `Region (${region}) or sub region (${subRegion}) doesn´t exists.`,
    } })
})

// fastify.post('/startRegionGame/', async (request, reply) => {
  
// })

// fastify.post('/startSubRegionGame/', async (request, reply) => {
  
// })

// fastify.post('/startCountriesGame/', async (request, reply) => {
  
// })

fastify.post('/startCountryGame', async (request, reply) => {
  const {
    name,
    region,
    subRegion,
    numPlayers = 2,
    citiesPerPlayer = 10,
  } = request.body

  const fileStat = await fsAsync.stat(`./data/${region}/${subRegion}.yaml`)

  if (fileStat && fileStat.isFile) {
    const subRegionData = yaml.load(await fsAsync.readFile(`./data/${region}/${subRegion}.yaml`, 'utf8'))
    const country = subRegionData.find(({ name: countryName }) => countryName == name)
    if (country) {
      if (country.cities.length / numPlayers >= citiesPerPlayer) {
        const selectedIndex = []
        const players = []

        for (let playerNumber = 0; playerNumber < numPlayers; playerNumber++) {
          const player = []
          for (let playerCardNumber = 0; playerCardNumber < citiesPerPlayer; playerCardNumber++) {
            const randomCardIndex = randomNumber(0, country.cities.length, selectedIndex)
            const { data: weatherData } = await weatherApi.get('/weather', {
              id: country.cities[randomCardIndex].openWeatherMapId,
              appid: process.env.OPENWEATHERMAP_API_KEY,
              units: 'metric',
              lang: 'en',
            })

            player.push({
              ...country.cities[randomCardIndex],
              weather: {
                icon: `https://openweathermap.org/img/wn/${weatherData.weather.icon}@2x.png`,
                temperatur: weatherData.main.temp,
                feelsLike: weatherData.main.feels_like,
                tempMin: weatherData.main.temp_min,
                tempMax: weatherData.main.temp_max,
                pressure: weatherData.main.pressure,
                humidity: weatherData.main.humidity,
                seaLevel: weatherData.main.sea_level,
                grndLevel: weatherData.main.grnd_level,
                visibility: weatherData.visibility,
                windSpeed: weatherData.wind.speed,
                windGust: weatherData.wind.gust,
                clouds: weatherData.clouds.all,
              },
            })
            selectedIndex.push(randomCardIndex)
          }
          players.push(player)
        }

        return {
          players,
          startPlayer: randomNumber(0, numPlayers - 1),
        }
      } else {
        reply
        .code(409)
        .send({ error: {
          message: `Country could not be found in data. (Region: ${region}, SubRegion: ${subRegion}, Country: ${name})`,
        } })
      }
    }
  }
  reply
    .code(404)
    .send({ error: {
      message: `Country has just ${country.cities.length} cities, so ${citiesPerPlayer} cities per player (Number of players: ${numPlayers}) cannot be generated.`,
    } })
})

// Run the server!
const start = async () => {
  if (process.env.OPENWEATHERMAP_API_KEY) {
    try {
      await fastify.listen(3000)
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
  } else {
    throw new Error('OPENWEATHERMAP_API_KEY environment variable is not set, you need to set an api key to run the server')
  }
}
start()