module.exports = function(min, max, blacklist) {
	min = min || 0;
	max = max || 0;

	var random = Math.floor(Math.random() * (max - min + 1)) + min;
	if (!blacklist || blacklist.constructor !== Array)
		return random;
	else if (typeof blacklist == 'string')
		blacklist = [ blacklist ];

	return blacklist.filter(function(number) { return number == random; }).lenght ? random_number(min, max, blacklist) : random;
}